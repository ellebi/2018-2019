/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        
 *  Configuration header for the PID_printInertialData_ch file.
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef BMI_160_CH_H_
#define BMI_160_CH_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */
#define PID_THREESECONDDELAY  UINT32_C(3000)      /** one second is represented by this macro */
#define PID_TIMERBLOCKTIME  UINT32_C(0xffff)    /** Macro used to define blocktime of a timer*/

/* local function prototype declarations */
/**
 * @brief Gets the data from BMI Accel and prints through the USB printf on serial port
 * @param[in] pvParameters Rtos task should be defined with the type void *(as argument)
 */
void bmi160_getSensorValues(void *pvParameters);
/* local module global variable declarations */

/* local inline function definitions */

#endif /* BMI_160_CH_H_ */

/** ************************************************************************* */
