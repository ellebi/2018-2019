// ex http://www.arduino.cc/en/Tutorial/AnalogReadSerial

// TODO trovare mappa seria pinOut ESP32->Arduino

// BOARD=

/**
 * serve per:
 * - demo "PWM dei poveri"
 * - demo "negativa" di come fare scheduling
 * - demo lettura velocità di rotazione (prima con fotoresistenza poi con reed)
 * - interrupt
 * - demo PID
 */

// ESP32
#define ROTOR 12
#define PHOTO 15
#define REED 13

// per vedere solo il reed
//#define PLOTREED

// Digistump https://digistump.com/wiki/digispark/tutorials/connecting
//#define ROTOR 1

// Arduino
//#define ROTOR LED_BUILTIN

#define PERIOD 800
#define DUTY 100
#define DELAY 1000
#define ACTIVE true

void setup() {
    Serial.begin(115200);
    pinMode(ROTOR, OUTPUT);
    pinMode(REED, INPUT_PULLUP); // quindi il reed va messo fra pin e massa (sarà "attivo basso")

	// https://techtutorialsx.com/2017/09/30/esp32-arduino-external-interrupts/
	// e anche esempio GPIOinterrupt
	
    attachInterrupt(digitalPinToInterrupt(REED), reedActivation, FALLING);
    
    
    // timer interrupt: https://techtutorialsx.com/2017/10/07/esp32-arduino-timer-interrupts/
}


int maxV=0; // valore massimo (varia nel tempo, si adatta)

long TmaxV=0; // istante di raggiungimento del max
long PmaxV=0; // periodo, misurato via max

long Treed=0; // istante di contatto del reed
long Preed=0; // periodo, misurato via reed

long TchangeV=0; // istante di raggiungimento del cambio segno
long PchangeV=0; // periodo, misurato via cambio segno

int diffV=0;
int preDiffV=0;

int minV=4096; // valore minimo

int preV=0; // valore precedente
int sensorValue=0; // valore attuale

int reedCount=0;

void loop() {
    long m=millis()%PERIOD;

    preV=sensorValue;
    preDiffV=diffV;

    maxV--;
    minV++;

    sensorValue = analogRead(PHOTO);
    diffV=preV-sensorValue;

#ifdef PLOTREED
    Serial.println(digitalRead(REED)); // POLLING
#endif

    if(digitalRead(REED)==LOW) {
        //Serial.println("reed!");
        Preed=millis()-Treed; // periodo (misurato col reed)
        Treed=millis();
        //Serial.println(Preed);
    }


    if((diffV*preDiffV)<0) {
        PchangeV=millis()-TchangeV; // periodo (misurato col max)
        TchangeV=millis();
    }

    if(sensorValue>maxV) {
        maxV=sensorValue;
        PmaxV=millis()-TmaxV; // periodo (misurato col max)
        TmaxV=millis();
    }

    if(sensorValue<minV) minV=sensorValue;

#ifndef PLOTREED
    //if(m==0) {

		/*
    Serial.print(sensorValue);
    Serial.print(",");
    Serial.print(minV);
    Serial.print(",");
    Serial.print(maxV);
    Serial.print(",");
    */

    /*
    Serial.print(PmaxV);
    Serial.print(",");
    Serial.print(PchangeV);
    Serial.print(",");
    */

    //Serial.print(Preed);
    //Serial.print(",");
    Serial.print(digitalRead(REED));
    Serial.print(",");
    Serial.println(reedCount);
    //}
#endif

#ifdef ACTIVE
    if(m < DUTY)
        digitalWrite(ROTOR, HIGH);

    if(m > DUTY)
        digitalWrite(ROTOR, LOW);
#endif

    delay(DELAY);
}

// ISR
void reedActivation() {
	noInterrupts();
    reedCount++;
    //interrupts();
}


/* PROVE/AVANZI
int contatore=0;
void loop() {
    contatore++;
    int sensorValue = analogRead(PHOTO);
    // print out the value you read:
    Serial.println(sensorValue);
    //Serial.println(millis());
    delay(50);        // delay in between reads for stability
    if(contatore%10 == 0) {
        Serial.println("impulso");
        digitalWrite(12, HIGH);
        delay(50);
        digitalWrite(12, LOW);
        //delay(1000);
    }
}
*/
