# Link utili

File provvisorio per radunare i link utili presi dal gruppo [telegram](https://t.me/joinchat/ALnoPw8wxMVMet7DYU1NZQ).

## Shopping

* Arduino (non originale) starter kit con strumenti [[amazon](https://www.amazon.it/Freenove-Processing-Oscilloscope-Voltmeter-Components/dp/B0721B8228)]
* Sensori vari per Arduino [[link1 amazon](https://www.amazon.it/Elegoo-Sensore-Elettronici-Tutorial-Inglese/dp/B01JLZLU58), [link2 amazon](https://www.amazon.it/Quimat-Projects-Raspberry-Programming-Tutorials/dp/B01DBJ48J0)]

## Canali Telegram

* Embedded news [[telegram](https://t.me/embedded_news)]

## Software

* Electronic simulator on browser [[github](https://github.com/sharpie7/circuitjs1)]
* Simple 3D modeling on browser (di Autodesk) [[link](https://www.tinkercad.com/)]

## Documentazione varia

* http://www.nongnu.org/avr-libc/
